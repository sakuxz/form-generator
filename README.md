# form-generator

This project is powered by Vue CLI. For More detail of Vue CLI, please see https://cli.vuejs.org/.

## How to Develop

### Install Dependencies
```
npm i
```

### Launch Dev Server with Hot Reload
```
npm run serve
```

### Run ESLint
```
npm run lint
```

## How to Build

```
npm i
npm run build
```
