import { shallowMount } from '@vue/test-utils';
import Config from '@/components/formInputs/input/config.vue';
import '@/registerGlobalComponent';

describe('Config.vue', () => {
  it('renders props.config.type as select tag value when passed', () => {
    const type = 'text';
    const wrapper = shallowMount(Config, {
      propsData: {
        config: {
          type,
        },
      },
    });

    const typeSelect = wrapper.find('select');
    expect(typeSelect.element.value).toMatch(type);
  });

  it('changes props.config.type when setting select tag value', () => {
    const type = 'text';
    const wrapper = shallowMount(Config, {
      propsData: {
        config: {
          type,
        },
      },
    });

    const typeSelect = wrapper.find('select');
    typeSelect.setValue('number');
    expect(typeSelect.element.value).toMatch('number');
    expect(wrapper.vm.config.type).toMatch('number');
  });

  it('renders props.config.placeholder as input tag value when passed', () => {
    const placeholder = 'placeholder';
    const wrapper = shallowMount(Config, {
      propsData: {
        config: {
          placeholder,
        },
      },
    });

    const placeholderInput = wrapper.find('input');
    expect(placeholderInput.element.value).toMatch(placeholder);
  });

  it('changes props.config.placeholder when setting input tag value', () => {
    const placeholder = 'placeholder';
    const wrapper = shallowMount(Config, {
      propsData: {
        config: {
          placeholder,
        },
      },
    });

    const placeholderInput = wrapper.find('input');
    placeholderInput.setValue('new_placeholder');
    expect(placeholderInput.element.value).toMatch('new_placeholder');
    expect(wrapper.vm.config.placeholder).toMatch('new_placeholder');
  });
});
