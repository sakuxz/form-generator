import { shallowMount } from '@vue/test-utils';
import Input from '@/components/formInputs/input/index.vue';

describe('Input.vue', () => {
  it('renders props as input tag attributes when passed', () => {
    const type = 'text';
    const placeholder = 'placeholder';
    const id = 'id';
    const required = false;
    const wrapper = shallowMount(Input, {
      propsData: {
        type,
        placeholder,
        id,
        required,
      },
    });
    expect(wrapper.attributes().type).toMatch(type);
    expect(wrapper.attributes().placeholder).toMatch(placeholder);
    expect(wrapper.attributes().id).toMatch(id);
    expect(wrapper.attributes().required).toBe(undefined);
  });

  it('renders attribute required in input tag when passed props.required true', () => {
    const required = true;
    const wrapper = shallowMount(Input, {
      propsData: {
        required,
      },
    });
    expect(wrapper.attributes().required).toBe('required');
  });
});
