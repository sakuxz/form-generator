import component from './index.vue';
import config from './config.vue';
import initialData from './initialData';

export default {
  name: '單行輸入框',
  component,
  config,
  initialData,
};
